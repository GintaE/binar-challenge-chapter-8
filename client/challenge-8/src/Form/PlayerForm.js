import React from 'react'

export default function PlayerForm(props) {
    const { type } = props            // Variable untuk menampung jenis form -> (Create, Edit)
    let { method, action } = ""
    let data = {}

    if (type === "create") {
        method = "POST"
        action = "/register-player"
        data = {
            username: "",
            email: "",
            password: "",
            experience: "",
            lvl: ""
        }
    } else if (type === "edit") {
        method = "PUT"
        action = "/update-player"
        data = {
            username: "ginta",
            email: "ginta@gmail.com",
            password: "123456",
            experience: "70",
            lvl: "5"
        }
    }

    return (
        <>
            <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css"></link>
            <script src='/bootstrap/js/bootstrap.min.js'></script>

            <section >
                <div class="mask d-flex align-items-center h-100 gradient-custom-3">
                    <div class="container h-100">
                        <div class="row d-flex justify-content-center align-items-center h-100">
                            <div class="col-12 col-md-9 col-lg-7 col-xl-6">

                                <div class="card-body p-5">
                                    <h2 class="text-uppercase text-center mb-5">Player Form</h2>

                                    <form method={method} action={action}>

                                        <div class="form-outline mb-4">
                                            <input type="text" id="username" class="form-control form-control-lg" value={data.username} />
                                            <label class="form-label" for="username">Username </label>
                                        </div>

                                        <div class="form-outline mb-4">
                                            <input type="email" id="email" class="form-control form-control-lg" value={data.email} />
                                            <label class="form-label" for="email">Email</label>
                                        </div>

                                        <div class="form-outline mb-4">
                                            <input type="password" id="password" class="form-control form-control-lg" value={data.password} />
                                            <label class="form-label" for="password">Password</label>
                                        </div>

                                        <div class="form-outline mb-4">
                                            <input type="number" id="experience" class="form-control form-control-lg" value={data.experience} />
                                            <label class="form-label" for="experience">Experience</label>
                                        </div>

                                        <div class="form-outline mb-4">
                                            <input type="number" id="lvl" class="form-control form-control-lg" value={data.lvl} />
                                            <label class="form-label" for="lvl">Level</label>
                                        </div>

                                        <div class="d-flex justify-content-center">
                                            <button type="submit"
                                                class="btn btn-success btn-block btn-lg gradient-custom-4 text-body">Submit</button>
                                        </div>

                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}