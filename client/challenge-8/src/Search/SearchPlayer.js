import React, { Component } from 'react'

class SearchPlayer extends Component {
    render() {
        return (
            <>
                <form method="POST" action='#'>
                    <div class="col-md-6">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control" id="username" />
                    </div>
                    <div class="col-md-6">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" id="email" />
                    </div>
                    <div class="col-md-6">
                        <label for="experience" class="form-label">Experience</label>
                        <input type="number" class="form-control" id="experience" />
                    </div>
                    <div class="col-md-6">
                        <label for="lvl" class="form-label">Lvl</label>
                        <input type="number" class="form-control" id="lvl" />
                    </div>
                    <button type="submit" class="btn btn-outline-dark">Submit</button>
                </form>
            </>
        );
    }
}

export default SearchPlayer